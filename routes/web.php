<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::prefix('user')->group(function () {
    Route::post('/authenticate', 'Auth\UserController@authenticate');
    Route::post('/createOrUpdateProfile', 'Auth\UserController@createOrUpdateProfile');
});

//e1b4522ace51552a8e091f6eba7e06e4a92721d5d3f596c3b359a702fd2a8074
