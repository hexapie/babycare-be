<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\UserProfile;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
//use Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function authenticate(Request $request){
		$success		= false;
		$validation		= false;
		$message 		= 'Unable to log in to the system';
		$token			= '';
        $token_expiry	= '';
        $is_signin      = false;
        $auth_type      = 'MN';
        $user           = null;
        
		
        $email 	        = $request->input('email');
        $user_id 	    = $request->input('user_id');
        $password 	    = $request->input('password');
        $is_signin      = $request->input('is_signin');
        $auth_type      = $request->input('auth_type'); //GM = Google+, FB = Facebook, MN = Manual

        $count          = User::where('email','=',$email)->count();
        
		
		if(empty($email)){
			$message = 'Email is required';
		}else if(empty($password)){
			$message = 'Password is required';
        }else{
			$validation = true;
        }
        
		
		if($validation){

            if($count>0){
                $credentials = $request->only('email', 'password');
                if(Auth::attempt($credentials)){
                    $user = User::where('email','=',$email)->first();
                    if($user->is_active){
                        $token = bin2hex(openssl_random_pseudo_bytes(32));
                        $token_expiry = date('Y-m-d H:i:s', strtotime('+1 day', strtotime(date('Y-m-d H:i:s'))));
                        $user->auth_token 	= $token;
                        $user->auth_expiry = $token_expiry;
                        $user->last_signin = date('Y-m-d H:i:s');
                        
                        if($user->save()){
                            $success = true;
                            $message = "Successfully login to the system";
                        }
                    }else{
                        $message = "Your account is not active";
                    }
                }else{
                    $message = "Invalid authentication information";
                }
            }else{
                
                $user = new User();
                $user->user_id 		    = $user_id;
                $user->email 		    = $email;
                $user->password		    = Hash::make($password);
                $user->auth_type 	    = $auth_type;
                $user->is_first_time    = true;
                $user->is_active 		= true;
                
                $token = bin2hex(openssl_random_pseudo_bytes(32));
                $token_expiry = date('Y-m-d H:i:s', strtotime('+1 day', strtotime(date('Y-m-d H:i:s'))));
                $user->auth_token 	= $token;
                $user->auth_expiry = $token_expiry;
                $user->last_signin = date('Y-m-d H:i:s');
               
                

                
                if($user->save()){
                    if($auth_type === 'MN' || empty($user_id)){
                        $user->user_id = $user->id;
                        $user->save();
                    }

                    $success	= true;
                    $message	= 'Successfully create user';
                    
                }
                
            }
            if($user){
                $profile = UserProfile::where('uid','=',$user->id)->count();
            
                if($profile > 0){
                    $user->is_first_time = 0;
                    $user->save();
                }
            }

        }

        $response = array('success'=>$success,'message'=>$message,'user'=> $user);
		$response = response()->json($response);
		$response->header('Content-Type', 'application/json');
		$response->header('Access-Control-Allow-Origin', '*');
		return $response;
    }

    public function createOrUpdateProfile(Request $request){
        $success		= false;
		$validation		= false;
        $message 		= 'Unable to create or update profile';
        $user           = null;
        $profile        = null;

        

        $uid            = $request->input('uid');

        $user = User::find($uid);
            
        $token          = $request->input('auth_token');
        $fullname 	    = $request->input('fullname');
        $dob 	        = $request->input('dob');
        $ic 	        = $request->input('ic');
        $address        = $request->input('address');
        $zipcode 	    = $request->input('zipcode');
        $city 	        = $request->input('city');
        $country 	    = $request->input('country');
        $trustee_id     = $request->input('trustee_id');
        $trustee_org 	= $request->input('trustee_org');
        $ic_picture     = $request->file('ic_picture');

        // var_dump($ic_picture);
        // exit;

        if(empty($uid)){
            $message = 'UID is required';
		}else if(empty($token)){
			$message = 'Invalid authentication due to missing authentication info';
        }else if(empty($fullname)){
            $message = 'Fullname is required';
        }else if(empty($dob)){
            $message = 'DOB is required';
        }else if(empty($ic)){
            $message = 'IC number is required';
        }else if(empty($address)){
            $message = 'Address is required';
        }else if(empty($zipcode)){
            $message = 'zipcode is required';
        }else if(empty($city)){
            $message = 'City is required';
        }else if(empty($country)){
            $message = 'Country is required';
        }else{
			$validation = true;
        }
        
        if($validation){
            $user = User::find($uid);

            if($user){
                if($user->is_active){

                    if($token == $user->auth_token){
                        $now = date('Y-m-d H:i:s');
                        
                        if($now < $user->auth_expiry){
        
                            $profile = UserProfile::where('uid','=',$uid)->first();
    
                            if($profile === null){
                                $profile = new UserProfile();
                            }

                            $dob = date_create($dob);
                            $dob = date_format($dob,"Y-m-d");
    
                            $profile->uid       = $uid;
                            $profile->fullname  = $fullname;
                            $profile->dob       = $dob;
                            $profile->ic_number = $ic;
                            $profile->address   = $address;
                            $profile->zipcode   = $zipcode;
                            $profile->city      = $city;
                            $profile->country   = $country;
                            $profile->trustee_id = $trustee_id;
                            $profile->trustee_org = $trustee_org;
                            $profile->created_by = $uid;
                            $profile->updated_by = $uid;

                            if($request->file('ic_picture')){
								
								$dest 	= public_path().'/uploads/';
								$dir 	= 'User_uploads_'.$user->id; 
								$ext	= $request->file('ic_picture')->getClientOriginalExtension();
								
								if(!file_exists($dest.$dir)){
									mkdir($dest.$dir);
									//dd('here');
								}
								
								
								
								$filename = 'User_IC_img@'.$user->id.'.'.$ext;
								//dd($filename);
								if($request->file('ic_picture')->move($dest.$dir, $filename)){
                                    $profile->ic_picture  = $filename;

                                }
                            }

                            if($profile->save()){
                                $success = true;
                                $message = 'User profile has been successfully saved';
                            }
    
        
                        }else{
                            $message = 'Authentication token is expired';
                        }
                    }else{
                        $message = 'Authentication token is invalid';
                    }
                    
                }else{

                    $message = 'User is not active';
                }
                
            }else{
                $message = 'User is not found';
            }

        }
        
        $response = array('success'=>$success,'message'=>$message,'user'=> $user,'profile'=> $profile);
		$response = response()->json($response);
		$response->header('Content-Type', 'application/json');
		$response->header('Access-Control-Allow-Origin', '*');
		return $response;

    }
}